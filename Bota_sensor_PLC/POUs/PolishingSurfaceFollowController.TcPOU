﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.11">
  <POU Name="PolishingSurfaceFollowController" Id="{2a607099-046e-44d1-b1a3-1b814ab4f5f5}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK PolishingSurfaceFollowController
VAR_INPUT
	WrenchIn : ARRAY[1..6] OF REAL; // Offsetted wrench [N and Nm]
	EEPose : ARRAY[1..6] OF REAL;
	desiredPressure : REAL;
END_VAR
VAR_OUTPUT
	Twist : ARRAY[1..6] OF REAL; // [mm/s and °/s]
	bTwistOK : BOOL;
	bFinished : BOOL;
	bTrackForce : BOOL;
END_VAR
VAR
	eTrackingState : (MOVE_TO_CONTACT, TRACK_FORCE, OVERLOAD);

	// Filter
	bResetFilter : BOOL := TRUE;
	alphaFilter : REAL := 1.0;
	WrenchFiltered : ARRAY[1..6] OF REAL;
	
	// Tracking controller
	WrenchDesired : ARRAY[1..6] OF REAL;
	speed_tr : REAL := 5;
	speed_scaling : REAL := 1.0;
	contactDirection : ARRAY[1..3] OF REAL := [0.0,0.0,1.0];
	normalEstimated : ARRAY[1..3] OF REAL;
	normalEstimatedTemp : ARRAY[1..3] OF REAL;
	normalMotion : ARRAY[1..3] OF REAL;
	motionDirection : ARRAY[1..3] OF REAL;
	motionRotAngle : REAL;
	motionDirectionWRF : ARRAY[1..3] OF REAL;
	xyzRot : ARRAY[1..3] OF REAL;
	pressure : REAL;
	
	TwistPID : ARRAY[1..6] OF FB_BasicPID;
	RotAnglePID : FB_BasicPID;
	fPID_output : LREAL;
	nErrorPID : UINT;
	
	fKp_Tr : REAL := 1.0;
	fTn_Tr : REAL := 0.0;
	fTv_Tr : REAL := 4.0;
	fTd_Tr : REAL := 25.0;
	
	fKp_Rot : REAL := 4.0;
	fTn_Rot : REAL := 5.0;
	fTv_Rot : REAL := 8.0;
	fTd_Rot : REAL := 25.0;
	
	fKp_RotAngle : REAL := 0.4;
	fTn_RotAngle : REAL := 100.0;
	fTv_RotAngle : REAL := 4.0;
	fTd_RotAngle : REAL := 0.0;
	
	// helper
	nCounter : INT;
	fPressureError : REAL;
	bMovePositiveY_TRF : BOOL := TRUE;
	
	l_x : REAL := 0.105; //0.08;
	l_z : REAL := 0.025;
	start_x : REAL;
	max_x : REAL := 180;
	min_x : REAL := 140;
	min_z : REAL := 102;
	z_tip : REAL;
	bReachedMaxX : BOOL;
	bReachedMinX : BOOL;
	bWasCloseStartX : BOOL;
	bToolIsRotating : BOOL := TRUE;
	rotationOffset : REAL := 0.0;//LREAL_TO_REAL(DEG_TO_RAD(-20.0));
	
END_VAR
VAR CONSTANT
	F_XY_CONTACT_THRESHOLD : REAL := 1.0;
	F_Z_CONTACT_THRESHOLD : REAL := 1.0;
	M_XY_CONTACT_THRESHOLD : REAL := 0.1;
	M_Z_CONTACT_THRESHOLD : REAL := 0.1;
	
	F_OVERLOAD : REAL := 30;
	M_OVERLOAD : REAL := 1.5;
	cycleTime : REAL := 0.00125;
	
	F_FORWARD_ACC : REAL := 0.0025; // delta v / cycle time
	
	MAX_TWIST_TR : REAL := 60.0; // [mm/s]
	MAX_TWIST_ROT : REAL := 45.0; // [°/s]
	
	l_x_tip : REAL := 95;
	l_z_tip : REAL := 63;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[IF bResetFilter THEN
	WrenchFiltered := WrenchIn;
	bResetFilter := FALSE;
	normalEstimated[2] := 0.0;
	normalEstimated[3] := -1.0;
	WrenchDesired[2] := 0.0;
	WrenchDesired[3] := -desiredPressure;
	motionDirection[2] := 1.0;
	motionDirection[3] := 0.0;
	start_x := EEPose[1];
	bReachedMaxX := FALSE;
	bReachedMinX := FALSE;
	bWasCloseStartX := TRUE;
	bMovePositiveY_TRF := TRUE;
	bTrackForce := FALSE;
	bFinished := FALSE;
END_IF

// Filter input
bTwistOK := TRUE;
FOR nCounter := 1 TO 6 DO
	WrenchFiltered[nCounter] := WrenchFiltered[nCounter] + alphaFilter * (WrenchIn[nCounter] - WrenchFiltered[nCounter]); 
END_FOR

// Estimate normal and apply pressure along normal
pressure := LREAL_TO_REAL(SQRT(EXPT(WrenchFiltered[2],2) + EXPT(WrenchFiltered[3],2)));
IF pressure > 0.95*desiredPressure THEN
	speed_scaling := 1.0;
	FOR nCounter := 2 TO 3 DO
		normalEstimated[nCounter] := WrenchFiltered[nCounter]/pressure;
		WrenchDesired[nCounter] := normalEstimated[nCounter] * desiredPressure;
	END_FOR
	normalEstimatedTemp := normalEstimated;
	normalEstimated[2] := COS(rotationOffset)*normalEstimatedTemp[2] - SIN(rotationOffset)*normalEstimatedTemp[3];
	normalEstimated[3] := SIN(rotationOffset)*normalEstimatedTemp[2] + COS(rotationOffset)*normalEstimatedTemp[3];
	normalMotion := normalEstimated;
	
	RotAnglePID(fSetpointValue := desiredPressure,
		  fActualValue := pressure, 
		  bReset := TRUE,
		  fCtrlCycleTime := cycleTime,
		  fKp := fKp_RotAngle,
		  fTn := fTn_RotAngle,
		  fTv := fTv_RotAngle,
		  fTd := fTd_RotAngle,
		  fCtrlOutput => fPID_output,
		  nErrorStatus => nErrorPID
		  );
ELSE
	IF pressure > 0.3*desiredPressure THEN
		FOR nCounter := 2 TO 3 DO
			normalEstimated[nCounter] := WrenchFiltered[nCounter]/pressure;
			WrenchDesired[nCounter] := normalEstimated[nCounter] * desiredPressure;
		END_FOR
		normalEstimatedTemp := normalEstimated;
		normalEstimated[2] := COS(rotationOffset)*normalEstimatedTemp[2] - SIN(rotationOffset)*normalEstimatedTemp[3];
		normalEstimated[3] := SIN(rotationOffset)*normalEstimatedTemp[2] + COS(rotationOffset)*normalEstimatedTemp[3];
	END_IF

	RotAnglePID(fSetpointValue := desiredPressure,
	  fActualValue := pressure, 
	  bReset := FALSE,
	  fCtrlCycleTime := cycleTime,
	  fKp := fKp_RotAngle,
	  fTn := fTn_RotAngle,
	  fTv := fTv_RotAngle,
	  fTd := fTd_RotAngle,
	  fCtrlOutput => fPID_output,
	  nErrorStatus => nErrorPID
	  );
  	motionRotAngle := LREAL_TO_REAL(fPID_output);
		  
	IF eTrackingState = TRACK_FORCE THEN
		IF bMovePositiveY_TRF THEN
			normalMotion[2] := COS(motionRotAngle)*normalEstimated[2] - SIN(motionRotAngle)*normalEstimated[3];
			normalMotion[3] := SIN(motionRotAngle)*normalEstimated[2] + COS(motionRotAngle)*normalEstimated[3];
		ELSE
			normalMotion[2] := COS(-motionRotAngle)*normalEstimated[2] - SIN(-motionRotAngle)*normalEstimated[3];
			normalMotion[3] := SIN(-motionRotAngle)*normalEstimated[2] + COS(-motionRotAngle)*normalEstimated[3];
		END_IF
		//normalEstimated := normalMotion;
	END_IF
END_IF

// Move along TRF y-axis (WRF x-axis)
xyzRot[1] := EEPose[4];
xyzRot[2] := EEPose[5];
xyzRot[3] := EEPose[6];

RotateVecFromTRFtoWRF(xyz_rot := xyzRot, vIn := motionDirection, vOut => motionDirectionWRF);
z_tip := EEPose[3] + LREAL_TO_REAL(SIN(DEG_TO_RAD(WrapAngle(EEPose[4] - 180))))*l_x_tip;

IF EEPose[1] > max_x AND bWasCloseStartX THEN
	bMovePositiveY_TRF := NOT bMovePositiveY_TRF;
	bReachedMaxX := TRUE;
	bWasCloseStartX := FALSE;
END_IF
IF EEPose[1] < min_x AND bWasCloseStartX THEN
	bMovePositiveY_TRF := NOT bMovePositiveY_TRF;
	bReachedMinX := TRUE;
	bWasCloseStartX := FALSE;
END_IF
IF EEPose[1] < start_x AND (z_tip < min_z OR EEPose[3] < 99) AND bWasCloseStartX THEN
	bMovePositiveY_TRF := NOT bMovePositiveY_TRF;
	bReachedMinX := TRUE;
	bWasCloseStartX := FALSE;
END_IF
IF EEPose[1] > start_x AND (z_tip < min_z OR EEPose[3] < 99) AND bWasCloseStartX THEN
	bMovePositiveY_TRF := NOT bMovePositiveY_TRF;
	bReachedMaxX := TRUE;
	bWasCloseStartX := FALSE;
END_IF
IF bMovePositiveY_TRF THEN
	motionDirection[2] := -normalMotion[3];
	motionDirection[3] := normalMotion[2];
ELSE
	motionDirection[2] := normalMotion[3];
	motionDirection[3] := -normalMotion[2];
END_IF
IF ABS(EEPose[1] - start_x) < 1.0 THEN
	bWasCloseStartX := TRUE;
END_IF
IF bReachedMaxX AND bReachedMinX AND bWasCloseStartX THEN
	bFinished := TRUE;
END_IF

// Tracking state
bTrackForce := FALSE;
CASE eTrackingState OF
	
	MOVE_TO_CONTACT:
		// Set twist
		FOR nCounter := 1 TO 3 DO
			Twist[nCounter] := contactDirection[nCounter]*5.0;
		END_FOR
		FOR nCounter := 4 TO 6 DO
			Twist[nCounter] := 0.0;
		END_FOR
		
		IF pressure > 0.5*desiredPressure OR pressure*l_x > MAX(0.5*desiredPressure*l_x, M_XY_CONTACT_THRESHOLD) THEN
		
			// Fy
			TwistPID[2](fSetpointValue := WrenchDesired[2],
					  fActualValue := WrenchFiltered[2], 
					  bReset := TRUE,
					  fCtrlCycleTime := cycleTime,
					  fKp := fKp_Tr,
					  fTn := fTn_Tr,
					  fTv := fTv_Tr,
					  fTd := fTd_Tr,
					  fCtrlOutput => fPID_output,
					  nErrorStatus => nErrorPID
					  );
		  	Twist[2] := LREAL_TO_REAL(-fPID_output);
			// Fz
			TwistPID[3](fSetpointValue := WrenchDesired[3],
					  fActualValue := WrenchFiltered[3], 
					  bReset := TRUE,
					  fCtrlCycleTime := cycleTime,
					  fKp := fKp_Tr,
					  fTn := fTn_Tr,
					  fTv := fTv_Tr,
					  fTd := fTd_Tr,
					  fCtrlOutput => fPID_output,
					  nErrorStatus => nErrorPID
					  );
			Twist[3] := LREAL_TO_REAL(-fPID_output);
					
			eTrackingState := TRACK_FORCE;
		END_IF
	
	TRACK_FORCE:
		// Fy
		TwistPID[2](fSetpointValue := WrenchDesired[2],
				  fActualValue := WrenchFiltered[2], 
				  bReset := FALSE,
				  fCtrlCycleTime := cycleTime,
				  fKp := fKp_Tr,
				  fTn := fTn_Tr,
				  fTv := fTv_Tr,
				  fTd := fTd_Tr,
				  fCtrlOutput => fPID_output,
				  nErrorStatus => nErrorPID
				  );
		Twist[2] := LREAL_TO_REAL(-fPID_output) + motionDirection[2]*speed_tr*speed_scaling;
		
		// Fz
		TwistPID[3](fSetpointValue := WrenchDesired[3],
				  fActualValue := WrenchFiltered[3], 
				  bReset := FALSE,
				  fCtrlCycleTime := cycleTime,
				  fKp := fKp_Tr,
				  fTn := fTn_Tr,
				  fTv := fTv_Tr,
				  fTd := fTd_Tr,
				  fCtrlOutput => fPID_output,
				  nErrorStatus => nErrorPID
				  );
		Twist[3] := LREAL_TO_REAL(-fPID_output) + motionDirection[3]*speed_tr*speed_scaling;
						  
		// Check if contact is lost
		// TODO: make this based on the estimated normal direction
		(*IF WrenchFiltered[3] > 3.0 THEN
			eTrackingState := MOVE_TO_CONTACT;
		END_IF*)
		
		// Check for overload
		FOR nCounter := 1 TO 3 DO
			IF ABS(WrenchFiltered[nCounter]) > F_OVERLOAD THEN
				eTrackingState := OVERLOAD;
			END_IF
		END_FOR
		FOR nCounter := 4 TO 6 DO
			IF ABS(WrenchFiltered[nCounter]) > M_OVERLOAD THEN
				eTrackingState := OVERLOAD;
			END_IF
		END_FOR
		bTrackForce := TRUE;
		
	OVERLOAD:
		FOR nCounter := 1 TO 6 DO
			Twist[nCounter] := 0.0;
		END_FOR
		bTwistOK := FALSE;
END_CASE

// Cap the maximum twist
FOR nCounter := 1 TO 3 DO
	Twist[nCounter] := LIMIT(-MAX_TWIST_TR, Twist[nCounter], MAX_TWIST_TR);
END_FOR
FOR nCounter := 4 TO 6 DO
	Twist[nCounter] := LIMIT(-MAX_TWIST_ROT, Twist[nCounter], MAX_TWIST_ROT);
END_FOR
]]></ST>
    </Implementation>
    <Method Name="ResetFilter" Id="{98eeb140-e48e-4647-95ba-674d69dbd0f6}">
      <Declaration><![CDATA[METHOD ResetFilter
]]></Declaration>
      <Implementation>
        <ST><![CDATA[bResetFilter := TRUE;
eTrackingState := MOVE_TO_CONTACT;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="setMinMaxX" Id="{8b9d7447-7f73-493f-9c03-cb664d494f39}">
      <Declaration><![CDATA[METHOD setMinMaxX
VAR_INPUT
	min_x_in : REAL;
	max_x_in : REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[max_x := max_x_in;
min_x := min_x_in;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="setMinZ" Id="{56f0b2b5-05ca-4c01-9e7e-454d0bd69e85}">
      <Declaration><![CDATA[METHOD setMinZ
VAR_INPUT
	min_z_in : REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[min_z := min_z_in;]]></ST>
      </Implementation>
    </Method>
    <Method Name="setMoveToContactDirection" Id="{5532679a-d36b-49fd-a0b3-71977ccda69e}">
      <Declaration><![CDATA[METHOD setMoveToContactDirection
VAR_INPUT
	contactDirectionIn : ARRAY[1..3] OF REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[contactDirection := contactDirectionIn;]]></ST>
      </Implementation>
    </Method>
    <Method Name="setSpeed" Id="{2913c027-ca4f-4f4d-a3b6-3187f7ee1e88}">
      <Declaration><![CDATA[METHOD setSpeed
VAR_INPUT
	speed_in : REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[speed_tr := speed_in;]]></ST>
      </Implementation>
    </Method>
    <Method Name="WrapAngle" Id="{2aef984b-e172-43b5-896b-bc42c751a4ca}">
      <Declaration><![CDATA[METHOD WrapAngle : REAL
VAR_INPUT
	angleIn : REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[WrapAngle := angleIn;
IF angleIn > 180 THEN
	WrapAngle := angleIn - 360;
END_IF
IF angleIn < -180 THEN
	WrapAngle := angleIn + 360;
END_IF]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="PolishingSurfaceFollowController">
      <LineId Id="937" Count="13" />
      <LineId Id="1532" Count="0" />
      <LineId Id="951" Count="16" />
      <LineId Id="1252" Count="2" />
      <LineId Id="968" Count="16" />
      <LineId Id="1258" Count="0" />
      <LineId Id="985" Count="0" />
      <LineId Id="1256" Count="1" />
      <LineId Id="1255" Count="0" />
      <LineId Id="986" Count="25" />
      <LineId Id="1016" Count="6" />
      <LineId Id="1541" Count="0" />
      <LineId Id="1023" Count="10" />
      <LineId Id="1427" Count="3" />
      <LineId Id="1426" Count="0" />
      <LineId Id="1435" Count="3" />
      <LineId Id="1434" Count="0" />
      <LineId Id="1034" Count="14" />
      <LineId Id="1533" Count="0" />
      <LineId Id="1049" Count="6" />
      <LineId Id="1443" Count="1" />
      <LineId Id="1442" Count="0" />
      <LineId Id="1057" Count="46" />
      <LineId Id="1109" Count="13" />
      <LineId Id="1138" Count="1" />
      <LineId Id="1439" Count="0" />
      <LineId Id="1140" Count="14" />
      <LineId Id="1534" Count="0" />
      <LineId Id="1155" Count="13" />
      <LineId Id="80" Count="0" />
      <LineId Id="1342" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.ResetFilter">
      <LineId Id="6" Count="0" />
      <LineId Id="5" Count="0" />
      <LineId Id="8" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.setMinMaxX">
      <LineId Id="5" Count="0" />
      <LineId Id="8" Count="0" />
      <LineId Id="14" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.setMinZ">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.setMoveToContactDirection">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.setSpeed">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="PolishingSurfaceFollowController.WrapAngle">
      <LineId Id="10" Count="5" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>