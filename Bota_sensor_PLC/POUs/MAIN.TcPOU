﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.12">
  <POU Name="MAIN" Id="{bbc3fb22-6fc4-4c7b-be9e-56a718a08411}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM MAIN
VAR
	//Create an instance of the MecaRobot
	robot : MecaInterface;
	torqueLimits : ARRAY[0..5] OF REAL := [100,100,100,100,100,100];
	desJointPos : ARRAY[0..5] OF REAL := [0,0,0,0,0,0];	
	storeJointPos : ARRAY[0..5] OF REAL := [0,-60,60,0,0,0];
	desPose : ARRAY[0..5] OF REAL := [0,0,0,0,0,0];
	EEPose : ARRAY[0..5] OF REAL := [0,0,0,0,0,0];
	nCounter : INT;
	bStart : BOOL := FALSE;
	bStop : BOOL := TRUE;
	bDeactivateRobot : BOOL := FALSE;
	bRepeat : BOOL := TRUE;
	bRetractFromPart : BOOL := FALSE;
	bGoZero : BOOL := FALSE;
	bHandGuiding : BOOL := FALSE;
	bPolishing : BOOL := FALSE;
	
	// Create a variable for the state machine
	MecaRobotState: INT:= -3;
	DesMotionState : INT := 150;//150;
	
	//Create an instance of the FT_sensor
	bft1: BotaForceTorqueSensorInterface;
	bSensorOK : BOOL;
	fWrench : ARRAY[1..6] OF REAL; // [N / Nm]
	fWrenchAveraged : ARRAY[1..6] OF REAL; // [N / Nm]
	fWrenchMaxRange : ARRAY[1..2, 1..6] OF REAL; // [N / Nm]
	fForceNorm : REAL;
	fTorqueNorm : REAL;
	bCompensateToolLoad : BOOL := FALSE;
	toolLoadCompensator : ToolLoadCompensation;
	
	// Twist command in TRF
	handGuideController : HandGuideWrenchToVel;
	forceTrackController : ForceToVel;
	trajController : VelTrajGenerator;
	rotTipController : RotVelTrajGenerator;
	polishingController : PolishingSurfaceFollowController;
	bControllerOK : BOOL;
	bTrajControllerOK : BOOL;
	bTrajFinished : BOOL;
	bIsForceTracking : BOOL := FALSE;
	fWrenchDesired : ARRAY[1..6] OF REAL := [0, 0, -10.0, 0, 0, 0]; // [N and Nm]
	fTwistTRF : ARRAY[1..6] OF REAL; // [mm/s and °/s]
	fTwistTrajTRF : ARRAY[1..6] OF REAL; // [mm/s and °/s]
	fSpeedScale : REAL;
	
	// Poses	
	poseInitPolish : ARRAY[0..5] OF REAL := [187,-16,155,180,0,-90];
	poseInnerPolish : ARRAY[0..5] OF REAL := [187,-16,115,180,0,-90];
	vCenterPoint : ARRAY[1..3] OF REAL;
	
	// Plotting
	plotForcePoints : ARRAY[1..3, 0..nPlotArraySize] OF ST_Point;
	plotTorquePoints : ARRAY[1..3, 0..nPlotArraySize] OF ST_Point;
	bInitPlottingArray : BOOL := FALSE;
	nTimeCounter : LINT := 0;
	nPlotSampleCounter : INT := 0;
END_VAR
VAR CONSTANT
	poseHandGuidance : ARRAY[0..5] OF REAL := [170,-50,180,180,0,-90];
	poseInitTrack : ARRAY[0..5] OF REAL := [170,-60,100,180,0,-90];
	poseInitFindX : ARRAY[0..5] OF REAL := [235,-16,117,180,0,-90];
	
	contactDirectionDown : ARRAY[1..3] OF REAL := [0.0,0.0,1.0];
	contactDirectionUp : ARRAY[1..3] OF REAL := [0.0,0.0,-1.0];
	// Plotting
	cycleTime : REAL := 0.00125;
	nPlotArraySize : INT := 800;
	nSamplesToAverage : INT := 10;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[bft1(bSensorOK => bSensorOK,
	 fWrenchOut => fWrench);
	 
IF bCompensateToolLoad THEN
	bft1.EnableOffset(bEnableOffset := FALSE);
	toolLoadCompensator(WrenchIn := fWrench, EEPose := robot.GetEEPose(), WrenchOut => fWrench);
END_IF
fForceNorm := LREAL_TO_REAL(SQRT(EXPT(fWrench[1],2) + EXPT(fWrench[2],2) + EXPT(fWrench[3],2)));
fTorqueNorm := LREAL_TO_REAL(SQRT(EXPT(fWrench[4],2) + EXPT(fWrench[5],2) + EXPT(fWrench[6],2)));

// Plotting data
IF NOT bInitPlottingArray THEN
	InitPlotting();
	bInitPlottingArray := TRUE;
END_IF

IF nTimeCounter MOD nSamplesToAverage = 0 THEN
	plotForcePoints[1,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotForcePoints[2,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotForcePoints[3,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotTorquePoints[1,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotTorquePoints[2,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotTorquePoints[3,nPlotSampleCounter].x := LINT_TO_REAL(nTimeCounter)*cycleTime;
	plotForcePoints[1,nPlotSampleCounter].y := fWrenchAveraged[1];
	plotForcePoints[2,nPlotSampleCounter].y := fWrenchAveraged[2];
	plotForcePoints[3,nPlotSampleCounter].y := fWrenchAveraged[3];
	plotTorquePoints[1,nPlotSampleCounter].y := fWrenchAveraged[4];
	plotTorquePoints[2,nPlotSampleCounter].y := fWrenchAveraged[5];
	plotTorquePoints[3,nPlotSampleCounter].y := fWrenchAveraged[6];
	
	FOR nCounter := 1 TO 6 DO
		fWrenchAveraged[nCounter] := 0.0;
	END_FOR
	nPlotSampleCounter := nPlotSampleCounter + 1;
END_IF

IF nPlotSampleCounter > nPlotArraySize THEN
	nPlotSampleCounter := 0;
END_IF

FOR nCounter := 1 TO 6 DO
	fWrenchAveraged[nCounter] := fWrenchAveraged[nCounter] + fWrench[nCounter]/INT_TO_REAL(nSamplesToAverage);
	IF fWrench[nCounter] > fWrenchMaxRange[2,nCounter] THEN
		fWrenchMaxRange[2,nCounter] := fWrench[nCounter];
	END_IF
	IF fWrench[nCounter] < fWrenchMaxRange[1,nCounter] THEN
		fWrenchMaxRange[1,nCounter] := fWrench[nCounter];
	END_IF
END_FOR

nTimeCounter := nTimeCounter + 1;

IF bStop THEN
	MecaRobotState:=-5;
	DesMotionState := 150;
//	bStart := FALSE;
END_IF
bHandGuiding := FALSE;

CASE MecaRobotState OF
	
	-5:
	// Do nothing
	bGoZero := FALSE;
	bRetractFromPart := FALSE;
	bHandGuiding := FALSE;
	bPolishing := FALSE;
	robot.ResetMotion();
	
	-3:
	// Reset motion
	robot.ResetMotion();
	MecaRobotState:=-2;
	
	-2:
	IF robot.ResetError() THEN
		MecaRobotState:=-1;
	END_IF
	
	-1:
	// Clear pause
	IF robot.ClearPause() THEN
		MecaRobotState:=0;
	END_IF
	
	0:
	// Reset Error
	IF robot.ResetError() THEN
		IF DesMotionState = 40 THEN
			bCompensateToolLoad := TRUE;
		ELSIF DesMotionState = 80 THEN
			bCompensateToolLoad := TRUE;
		ELSE
			bCompensateToolLoad := TRUE;
		END_IF
		MecaRobotState:=1;
		bPolishing := FALSE;
	END_IF
	
	1:
	//Activate the Robot
	IF robot.Activate() THEN	
		MecaRobotState:=2;
	END_IF
	
	2:
	//Home the Robot
	IF robot.Home() THEN
		MecaRobotState:=3;
	END_IF
	
	3:
	//Set the Joint Acc
	robot.ConfigJointAcc(jointAcc := 150);
	MecaRobotState:= 4;
	
	
	4:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 20);
	MecaRobotState:= 5;
		
	5:
	//Set the Joint Vel
	robot.ConfigJointVel(jointVel := 10);
	MecaRobotState:= 6;
	
	6:
	//Set the Cartesian Linear Vel
	robot.ConfigCartLinVel(cartLinVel := 50);
	MecaRobotState:= 7;
	
	7:
	//Set the velocity timeout
	robot.ConfigVelTimeout(velTimeout := 0.010);
	MecaRobotState:= 8;

	8:
	//Set the torque limits
	robot.ConfigTorqueLimits(torqueLimits := torqueLimits);
	MecaRobotState:= 9;
	
	9:
	//Set the torque limits config
	robot.ConfigTorqueLimitsCfg(eventSeverity := 2, detectionMode := 1);
	MecaRobotState:= 10;
	
	10:
	//Move to the zero pose
	FOR nCounter := 0 TO 5 DO
		desJointPos[nCounter] := 0.0;
	END_FOR
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 11;
	
	11:
	//Move to the initial pose
	IF DesMotionState = 40 THEN
		robot.MoveEEPose(pose := poseHandGuidance);
	ELSIF DesMotionState = 80 THEN
		FOR nCounter := 0 TO 5 DO
			desJointPos[nCounter] := 0.0;
		END_FOR
		desJointPos[5] := -90;
		robot.MoveJointPos(jointPos := desJointPos);
	ELSIF DesMotionState = 20 OR DesMotionState = 400 THEN
		// Do nothing
	ELSIF DesMotionState = 150 THEN
		robot.MoveEEPose(pose := poseInitFindX);
	ELSIF DesMotionState = 200 THEN
		robot.MoveEEPose(pose := poseInitPolish);
	ELSE
		robot.MoveEEPose(pose := poseInitTrack);
		polishingController.setMinMaxX(min_x_in := 160, max_x_in := 200);
	END_IF
	MecaRobotState:= 12;
		
	12:
	// Delay of 0.5 s before reset the sensor
	robot.AddDelay(duration := 0.5);
	MecaRobotState:= 13;
	
	13:
	// Clear pause
	IF robot.ClearPause() THEN
		MecaRobotState:=14;
	END_IF
	
	14:
	// Set a CheckPoint trigger to know when the robot has completed the previous move
	IF robot.WaitMotionEnd() THEN
		MecaRobotState:= 15;
	END_IF
	
	15:
	// Delay of 1.2 s to reset the sensor
	robot.AddDelay(duration := 1.0);
	IF bCompensateToolLoad THEN
		toolLoadCompensator.CalibrateOffset();
	ELSE	
		bft1.TriggerOffsetCalibration();
	END_IF
	MecaRobotState:= 16;
	
	16:
	IF robot.WaitMotionEnd() THEN
		handGuideController.ResetFilter();
		forceTrackController.ResetFilter();
		polishingController.ResetFilter();
		//polishingController.setMinMaxX(min_x_in := 140, max_x_in := 200);
		trajController.Reset();
		rotTipController.Reset();
		bControllerOK := TRUE;
		bRepeat := TRUE;
		MecaRobotState:= DesMotionState;
		bft1.ConfigFilter(fAlphaIn := 1.0);
	END_IF
	
	(* Go to initial state: zero joint position *)
	20:
	bGoZero := TRUE;
	bPolishing := FALSE;
	//Move to the zero position
	FOR nCounter := 0 TO 5 DO
		desJointPos[nCounter] := 0.0;
	END_FOR
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 21;
	
	21:
	IF robot.WaitMotionEnd() THEN
		MecaRobotState:= 100;
		bGoZero := FALSE;
	END_IF
		
	(* Retract motion *)
	30:
	//Retract motion
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -20;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 31;
	
	31:
	IF robot.WaitMotionEnd() THEN
		// Stop sending commands
		//MecaRobotState:= 100;
	END_IF
	
	(* Hand-guidance controller *)
	40:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	bft1.ConfigFilter(fAlphaIn := 0.9);
	MecaRobotState:= 41;
	
	41:
	//Switch to velocity mode
	handGuideController(WrenchIn := fWrench, Twist => fTwistTRF, bTwistOK => bControllerOK);
	bHandGuiding := TRUE;
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	
	(* Force-track controller *)
	50:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 51;
	
	51:
	//Switch to velocity mode
	forceTrackController(WrenchIn := fWrench, WrenchDesired := fWrenchDesired, Twist => fTwistTRF, bTwistOK => bControllerOK);
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	
	(* Force-track + velocity controller *)
	60:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 61;
	
	61:
	//Switch to velocity mode
	forceTrackController(WrenchIn := fWrench, WrenchDesired := fWrenchDesired, Twist => fTwistTRF, bTwistOK => bControllerOK);
	trajController(EEPose := robot.GetEEPose(), forceCtrlTwistTRF := fTwistTRF, fSpeedScale := 1.0, Twist => fTwistTrajTRF, bTwistOK => bTrajControllerOK);
	fTwistTRF[1] := fTwistTRF[1] + fTwistTrajTRF[1];
	fTwistTRF[2] := fTwistTRF[2] + fTwistTrajTRF[2];
	fTwistTRF[3] := fTwistTRF[3] + fTwistTrajTRF[3];
	fTwistTRF[4] := fTwistTRF[4] + fTwistTrajTRF[4];
	fTwistTRF[5] := fTwistTRF[5] + fTwistTrajTRF[5];
	fTwistTRF[6] := fTwistTRF[6] + fTwistTrajTRF[6];
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bTrajControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	
	(*Polishing application *)
	70:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 71;
	
	71:
	polishingController(WrenchIn := fWrench, EEPose := robot.GetEEPose(), desiredPressure := 3.0, Twist => fTwistTRF, bTwistOK => bControllerOK, bFinished => bTrajFinished);
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	IF bTrajFinished THEN
		MecaRobotState:= 72;
	END_IF
	
	72:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 73;
	
	73:
	//Move up
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -10;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	//MecaRobotState:= 100;
	
	(*
	74:
	//increase the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 100;
	*)
	
	(* Identification motion *)
	80:
	//Move to the zero position
	(*FOR nCounter := 0 TO 5 DO
		desJointPos[nCounter] := 0.0;
	END_FOR
	robot.MoveJointPos(jointPos := desJointPos);*)
	MecaRobotState:= 81;
	
	81:
	desJointPos[4] := 0;
	desJointPos[5] := -90;
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 82;
	
	82:
	desJointPos[4] := 90;
	desJointPos[5] := -90;
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 83;
		
	83:
	desJointPos[4] := -90;
	desJointPos[5] := -90;
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 84;
	
	84:
	desJointPos[4] := 0;
	desJointPos[5] := -90;
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 85;
	
	85:
	//Move to the zero position
	FOR nCounter := 0 TO 5 DO
		desJointPos[nCounter] := 0.0;
	END_FOR
	robot.MoveJointPos(jointPos := desJointPos);
	MecaRobotState:= 87;
	
	87:
	IF robot.WaitMotionEnd() THEN
		//MecaRobotState:= 100;
	END_IF
	
	100:
	// Set the SetPoint to 0 to stop sending commands to FIFO Buffer
	IF robot.Deactivate(bDeactivate := bDeactivateRobot) THEN
		MecaRobotState:= 101;
	END_IF
	
	101:
	// Do nothing
	
	(* Move until contact *)
	150:
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	bPolishing := TRUE;
	MecaRobotState:= 151;
	
	151:
	FOR nCounter := 1 TO 6 DO
		fTwistTRF[nCounter] := 0.0;
	END_FOR
	fTwistTRF[2] := -5.0;
	IF fForceNorm < 1.0 THEN
		robot.MoveEELinVelTRF(twist := fTwistTRF);
	ELSE
		EEPose := robot.GetEEPose();
		poseInitPolish[0] := EEPose[0] - 25;
		poseInnerPolish := poseInitPolish;
		poseInnerPolish[2] := poseInitPolish[2] - 36;
		DesMotionState := 200;
		fTwistTRF[2] := 0.0;		
		MecaRobotState:= 152;	
	END_IF
	
	152:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 153;
	
	153:
	//Move back
	desPose[0] := 0;
	desPose[1] := 15;
	desPose[2] := 0;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 154;
	
	154:
	//Move up
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -40;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 11;
	
	(* Watch polishing application *)
	200:
	// Set limits
	polishingController.setMinMaxX(min_x_in := 135, max_x_in := 245);
	polishingController.setMinZ(min_z_in := 103);
	polishingController.setSpeed(speed_in := 5);
	polishingController.setMoveToContactDirection(contactDirectionIn := contactDirectionDown);
	vCenterPoint[1] := poseInnerPolish[0];
	vCenterPoint[2] := poseInnerPolish[1];
	vCenterPoint[3] := poseInnerPolish[2];
	rotTipController.SetCenterPoint(vCenterPoint_in := vCenterPoint);
	
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 201;
	
	201:
	polishingController(WrenchIn := fWrench, EEPose := robot.GetEEPose(), desiredPressure := 3.0, Twist => fTwistTRF, bTwistOK => bControllerOK, bFinished => bTrajFinished, bTrackForce => bIsForceTracking);
	IF bIsForceTracking THEN
		rotTipController(EEPose := robot.GetEEPose(), fFrequency := 0.2, fAmplitude := 0.0, Twist => fTwistTrajTRF, bTwistOK => bTrajControllerOK);
		FOR nCounter := 1 TO 6 DO
			fTwistTRF[nCounter] := fTwistTRF[nCounter] + fTwistTrajTRF[nCounter];
		END_FOR
	ELSE
		bTrajControllerOK := TRUE;
	END_IF
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bTrajControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	IF bTrajFinished THEN
		MecaRobotState:= 202;
	END_IF
	
	202:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 203;
	
	203:
	//Move up
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -15;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 300;
	
	204:
	//Retract
	EEPose := robot.GetEEPose();
	desPose[0] := -40;
	desPose[1] := 0;
	desPose[2] := 0;
	desPose[3] := 0;
	desPose[4] := -WrapAngle(180 - EEPose[3]);
	desPose[5] := -WrapAngle(-90 - EEPose[5]);
	(*desPose[3] := WrapAngle(180 - EEPose[3]);
	desPose[4] := WrapAngle(0 - EEPose[4]);
	desPose[5] := WrapAngle(90 - EEPose[5]);*)
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 205;
	
	205:
	EEPose := poseInitPolish;
	EEPose[1] := poseInitPolish[1] - 30;
	EEPose[2] := poseInitPolish[2] - 40;
	robot.MoveEEPose(pose := EEPose);
	MecaRobotState:= 206;
	
	206:
	poseInnerPolish := poseInitPolish;
	poseInnerPolish[2] := poseInitPolish[2] - 40;
	robot.MoveEEPose(pose := poseInnerPolish);
	MecaRobotState:= 207;
	
	207:
	// Delay of 0.5 s before reset the sensor
	robot.AddDelay(duration := 0.5);
	MecaRobotState:= 208;
	
	208:
	// Set a CheckPoint trigger to know when the robot has completed the previous move
	IF robot.WaitMotionEnd() THEN
		MecaRobotState:= 209;
	END_IF
	
	209:
	// Delay of 1.2 s to reset the sensor
	robot.AddDelay(duration := 1.0);
	IF bCompensateToolLoad THEN
		toolLoadCompensator.CalibrateOffset();
	ELSE	
		bft1.TriggerOffsetCalibration();
	END_IF
	MecaRobotState:= 210;
	
	210:
	IF robot.WaitMotionEnd() THEN
		handGuideController.ResetFilter();
		forceTrackController.ResetFilter();
		polishingController.ResetFilter();
		//polishingController.setMinMaxX(min_x_in := 140, max_x_in := 200);
		trajController.Reset();
		rotTipController.Reset();
		bControllerOK := TRUE;
		MecaRobotState:= 211;
		bft1.ConfigFilter(fAlphaIn := 1.0);
	END_IF

	211:
	// Set limits
	polishingController.setMinMaxX(min_x_in := 140, max_x_in := 235);
	polishingController.setMinZ(min_z_in := 105);	
	polishingController.setSpeed(speed_in := 7);
	polishingController.setMoveToContactDirection(contactDirectionIn := contactDirectionUp);
	
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 212;
	
	212:
	polishingController(WrenchIn := fWrench, EEPose := robot.GetEEPose(), desiredPressure := 3.0, Twist => fTwistTRF, bTwistOK => bControllerOK, bFinished => bTrajFinished);
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 220;
	END_IF
	IF bTrajFinished THEN
		MecaRobotState:= 220;
	END_IF
	
	219:
	bRetractFromPart := TRUE;
	bRepeat := FALSE;
	bPolishing := FALSE;
	MecaRobotState:= 220;
	
	220:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 221;
	
	
	221:
	desPose[0] := 0;
	desPose[1] := 5.0*fWrench[2]/fForceNorm;
	desPose[2] := 5.0*fWrench[3]/fForceNorm;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 222;
	
	222:
	//Retract
	desPose[0] := -30;
	desPose[1] := 0;
	desPose[2] := 0;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 223;
	
	223:
	//Move up
//	desPose[0] := 0;
//	desPose[1] := 0;
//	desPose[2] := -50;
//	desPose[3] := 0;
//	desPose[4] := 0;
//	desPose[5] := 0;
//	robot.MoveEELinRelTRF(pose := desPose);
	IF robot.WaitMotionEnd() THEN
		bRetractFromPart := FALSE;
		IF bRepeat THEN
			DesMotionState := 150;
			MecaRobotState:= 11;
		ELSE		
			bPolishing := FALSE;
			MecaRobotState:= 20;
			DesMotionState := 150;
		END_IF
	END_IF

	230:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 231;
	
	231:
	//Move up
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -15;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 232;
	
	232:
	//Retract
	desPose[0] := -20;
	desPose[1] := 0;
	desPose[2] := 0;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 20;
	
	300:
	// Delay of 0.5 s before reset the sensor
	robot.AddDelay(duration := 0.5);
	MecaRobotState:= 301;
	
	301:
	// Set a CheckPoint trigger to know when the robot has completed the previous move
	IF robot.WaitMotionEnd() THEN
		MecaRobotState:= 302;
	END_IF
	
	302:
	// Delay of 1.2 s to reset the sensor
	robot.AddDelay(duration := 1.0);
	IF bCompensateToolLoad THEN
		toolLoadCompensator.CalibrateOffset();
	ELSE	
		bft1.TriggerOffsetCalibration();
	END_IF
	MecaRobotState:= 303;
	
	303:
	IF robot.WaitMotionEnd() THEN
		handGuideController.ResetFilter();
		forceTrackController.ResetFilter();
		polishingController.ResetFilter();
		//polishingController.setMinMaxX(min_x_in := 140, max_x_in := 200);
		trajController.Reset();
		rotTipController.Reset();
		bControllerOK := TRUE;
		MecaRobotState:= 304;
		bft1.ConfigFilter(fAlphaIn := 1.0);
	END_IF

	304:
	// Set limits
	polishingController.setMinMaxX(min_x_in := 110, max_x_in := 300);
	polishingController.setMinZ(min_z_in := 97);
	polishingController.setSpeed(speed_in := 3);
	polishingController.setMoveToContactDirection(contactDirectionIn := contactDirectionDown);
	vCenterPoint[1] := poseInnerPolish[0];
	vCenterPoint[2] := poseInnerPolish[1];
	vCenterPoint[3] := poseInnerPolish[2];
	rotTipController.SetCenterPoint(vCenterPoint_in := vCenterPoint);
	
	//Set the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 600);
	MecaRobotState:= 305;
	
	305:
	polishingController(WrenchIn := fWrench, EEPose := robot.GetEEPose(), desiredPressure := 3.0, Twist => fTwistTRF, bTwistOK => bControllerOK, bFinished => bTrajFinished, bTrackForce => bIsForceTracking);
	IF bIsForceTracking THEN
		rotTipController(EEPose := robot.GetEEPose(), fFrequency := 0.2, fAmplitude := 10.0, Twist => fTwistTrajTRF, bTwistOK => bTrajControllerOK);
		FOR nCounter := 1 TO 6 DO
			fTwistTRF[nCounter] := fTwistTRF[nCounter] + fTwistTrajTRF[nCounter];
		END_FOR
	ELSE
		bTrajControllerOK := TRUE;
	END_IF
	robot.MoveEELinVelTRF(twist := fTwistTRF);
	IF NOT bControllerOK OR NOT bTrajControllerOK OR NOT bSensorOK THEN
		MecaRobotState:= 30;
	END_IF
	IF bTrajFinished THEN
		MecaRobotState:= 306;
	END_IF
	
	306:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 307;
	
	307:
	//Move up
	desPose[0] := 0;
	desPose[1] := 0;
	desPose[2] := -15;
	desPose[3] := 0;
	desPose[4] := 0;
	desPose[5] := 0;
	robot.MoveEELinRelTRF(pose := desPose);
	MecaRobotState:= 204;
	
	400:
	//reduce the Cartesian Acc
	robot.ConfigCartAcc(cartAcc := 50);
	MecaRobotState:= 401;
	
	401:
	//Set the Joint Vel
	robot.ConfigJointVel(jointVel := 5);
	MecaRobotState:= 402;
	
	402:
	robot.MoveJointPos(jointPos := storeJointPos);
	MecaRobotState:= 403;
	
	403:	
	bDeactivateRobot := TRUE;
	IF robot.WaitMotionEnd() THEN
		//MecaRobotState:= 100;
	END_IF
	
END_CASE
]]></ST>
    </Implementation>
    <Method Name="InitPlotting" Id="{c265be52-ec0c-4fd4-a180-cbc0947061a3}">
      <Declaration><![CDATA[METHOD InitPlotting
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[nTimeCounter := 0;
nPlotSampleCounter := 0;

FOR nCounter := 1 TO 6 DO
	fWrenchAveraged[nCounter] := 0.0;
	fWrenchMaxRange[1,nCounter] := 0.0;
	fWrenchMaxRange[2,nCounter] := 0.0;
END_FOR

FOR nCounter := 0 TO nPlotArraySize DO
	plotForcePoints[1,nCounter].x := 0.0;
	plotForcePoints[1,nCounter].y := 0.0;
	plotForcePoints[2,nCounter].x := 0.0;
	plotForcePoints[2,nCounter].y := 0.0;
	plotForcePoints[3,nCounter].x := 0.0;
	plotForcePoints[3,nCounter].y := 0.0;
	plotTorquePoints[1,nCounter].x := 0.0;
	plotTorquePoints[1,nCounter].y := 0.0;
	plotTorquePoints[2,nCounter].x := 0.0;
	plotTorquePoints[2,nCounter].y := 0.0;
	plotTorquePoints[3,nCounter].x := 0.0;
	plotTorquePoints[3,nCounter].y := 0.0;
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="WrapAngle" Id="{4a068369-10b7-42c2-84fe-418d990ae726}">
      <Declaration><![CDATA[METHOD WrapAngle : REAL
VAR_INPUT
	angleIn : REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[WrapAngle := angleIn;
IF angleIn > 180 THEN
	WrapAngle := angleIn - 360;
END_IF
IF angleIn < -180 THEN
	WrapAngle := angleIn + 360;
END_IF]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>