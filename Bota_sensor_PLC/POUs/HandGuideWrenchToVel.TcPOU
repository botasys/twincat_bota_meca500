﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.11">
  <POU Name="HandGuideWrenchToVel" Id="{5bb20fa3-466c-4a69-a09c-29eb61c8df89}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK HandGuideWrenchToVel
VAR_INPUT
	WrenchIn : ARRAY[1..6] OF REAL; // Offsetted wrench [N and Nm]
END_VAR
VAR_OUTPUT
	Twist : ARRAY[1..6] OF REAL; // [mm/s and °/s]
	bTwistOK : BOOL;
END_VAR
VAR
	bResetFilter : BOOL := TRUE;
	alphaFilter : REAL := 0.8;
	WrenchFiltered : ARRAY[1..6] OF REAL;
	
	eMotionGroup : (NONE, TRANSLATION, ROTATION);
	
	normF : LREAL;
	normM : LREAL;
	
	nCounter : INT;
	tempDeadZone : REAL;
END_VAR
VAR CONSTANT
	DEAD_ZONE_M : REAL := 0.02;
	
	F_THRESHOLD_HIGH : REAL := 0.15;
	F_THRESHOLD_LOW : REAL := 0.05;
	M_THRESHOLD_HIGH : REAL := 0.3;
	M_THRESHOLD_LOW : REAL := 0.05;
	
	GAIN_TR : REAL := 10;//15.0;
	GAIN_ROT : REAL := 60;//75.0;
	
	MAX_TWIST_TR : REAL := 60.0; // [mm/s]
	MAX_TWIST_ROT : REAL := 45.0; // [°/s]
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[IF bResetFilter THEN
	WrenchFiltered := WrenchIn;
	bResetFilter := FALSE;
END_IF

// Filter input
FOR nCounter := 1 TO 6 DO
	WrenchFiltered[nCounter] := WrenchFiltered[nCounter] + alphaFilter * (WrenchIn[nCounter] - WrenchFiltered[nCounter]); 
END_FOR

// Set twist to zero
bTwistOK := TRUE;

// Get twist gain
normF := SQRT(EXPT(WrenchFiltered[1],2) + EXPT(WrenchFiltered[2],2) + EXPT(WrenchFiltered[3],2));
normM := SQRT(EXPT(WrenchFiltered[4],2) + EXPT(WrenchFiltered[5],2) + EXPT(WrenchFiltered[6],2));

IF normF <= F_THRESHOLD_LOW AND normM <= M_THRESHOLD_LOW THEN
	eMotionGroup := NONE;
END_IF

IF normM > M_THRESHOLD_HIGH THEN
	eMotionGroup := ROTATION;
ELSE
	IF eMotionGroup = ROTATION AND normM > M_THRESHOLD_LOW THEN
		eMotionGroup := ROTATION;
	ELSIF normF > F_THRESHOLD_HIGH THEN
		eMotionGroup := TRANSLATION;
	ELSIF eMotionGroup = ROTATION AND normM < M_THRESHOLD_LOW THEN
		eMotionGroup := NONE;
	ELSIF eMotionGroup = TRANSLATION AND normF < F_THRESHOLD_LOW THEN
		eMotionGroup := NONE;
	END_IF
END_IF

CASE eMotionGroup OF
	NONE:
		FOR nCounter := 1 TO 6 DO
			Twist[nCounter] := 0.0; 
		END_FOR
		
	TRANSLATION:
		FOR nCounter := 1 TO 3 DO
			tempDeadZone := LREAL_TO_REAL(WrenchFiltered[nCounter]/normF*F_THRESHOLD_LOW);
			Twist[nCounter] := GAIN_TR*(WrenchFiltered[nCounter] - tempDeadZone);
		END_FOR
		FOR nCounter := 4 TO 6 DO
			Twist[nCounter] := 0.0;
		END_FOR
	
	ROTATION:
		FOR nCounter := 1 TO 3 DO
			Twist[nCounter] := 0.0;
		END_FOR		
		FOR nCounter := 4 TO 6 DO
			tempDeadZone := LREAL_TO_REAL(WrenchFiltered[nCounter]/normM*M_THRESHOLD_LOW);
			Twist[nCounter] := GAIN_ROT*(WrenchFiltered[nCounter] - tempDeadZone);
		END_FOR

END_CASE

// Cap the maximum twist
FOR nCounter := 1 TO 3 DO
	Twist[nCounter] := LIMIT(-MAX_TWIST_TR, Twist[nCounter], MAX_TWIST_TR);
END_FOR
FOR nCounter := 4 TO 6 DO
	Twist[nCounter] := LIMIT(-MAX_TWIST_ROT, Twist[nCounter], MAX_TWIST_ROT);
END_FOR

IF normF > 20.0 THEN
	bTwistOK := FALSE;
END_IF
]]></ST>
    </Implementation>
    <Method Name="ResetFilter" Id="{5d2c32ea-74a1-4a33-8bb0-b4111e1623b8}">
      <Declaration><![CDATA[METHOD ResetFilter
]]></Declaration>
      <Implementation>
        <ST><![CDATA[bResetFilter := TRUE;
eMotionGroup := NONE;
]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="HandGuideWrenchToVel">
      <LineId Id="9" Count="0" />
      <LineId Id="24" Count="0" />
      <LineId Id="26" Count="0" />
      <LineId Id="25" Count="0" />
      <LineId Id="42" Count="4" />
      <LineId Id="161" Count="0" />
      <LineId Id="127" Count="0" />
      <LineId Id="86" Count="0" />
      <LineId Id="165" Count="0" />
      <LineId Id="28" Count="0" />
      <LineId Id="27" Count="0" />
      <LineId Id="33" Count="0" />
      <LineId Id="172" Count="0" />
      <LineId Id="171" Count="0" />
      <LineId Id="175" Count="1" />
      <LineId Id="189" Count="0" />
      <LineId Id="188" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="202" Count="0" />
      <LineId Id="206" Count="0" />
      <LineId Id="208" Count="0" />
      <LineId Id="210" Count="1" />
      <LineId Id="215" Count="3" />
      <LineId Id="209" Count="0" />
      <LineId Id="191" Count="0" />
      <LineId Id="177" Count="0" />
      <LineId Id="35" Count="0" />
      <LineId Id="178" Count="0" />
      <LineId Id="185" Count="1" />
      <LineId Id="181" Count="0" />
      <LineId Id="187" Count="0" />
      <LineId Id="182" Count="0" />
      <LineId Id="219" Count="2" />
      <LineId Id="183" Count="0" />
      <LineId Id="224" Count="0" />
      <LineId Id="226" Count="0" />
      <LineId Id="223" Count="0" />
      <LineId Id="222" Count="0" />
      <LineId Id="184" Count="0" />
      <LineId Id="235" Count="1" />
      <LineId Id="228" Count="0" />
      <LineId Id="234" Count="0" />
      <LineId Id="229" Count="0" />
      <LineId Id="237" Count="0" />
      <LineId Id="231" Count="0" />
      <LineId Id="227" Count="0" />
      <LineId Id="179" Count="0" />
      <LineId Id="85" Count="0" />
      <LineId Id="84" Count="0" />
      <LineId Id="89" Count="0" />
      <LineId Id="91" Count="0" />
      <LineId Id="87" Count="0" />
      <LineId Id="96" Count="1" />
      <LineId Id="95" Count="0" />
      <LineId Id="129" Count="2" />
      <LineId Id="128" Count="0" />
      <LineId Id="273" Count="0" />
    </LineIds>
    <LineIds Name="HandGuideWrenchToVel.ResetFilter">
      <LineId Id="5" Count="0" />
      <LineId Id="7" Count="0" />
      <LineId Id="9" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>