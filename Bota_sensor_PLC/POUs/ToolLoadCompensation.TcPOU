﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.11">
  <POU Name="ToolLoadCompensation" Id="{e2aaf57f-3952-4a5e-9dfd-5b411ddd2d2a}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK ToolLoadCompensation
VAR_INPUT
	WrenchIn : ARRAY[1..6] OF REAL; // Measured wrench [N and Nm]
	EEPose : ARRAY[1..6] OF REAL;
END_VAR
VAR_OUTPUT
	WrenchOut : ARRAY[1..6] OF REAL; // Measured wrench [N and Nm]
END_VAR
VAR
	// Inertia parameters
	fMass : REAL := 0.227;
	fCoM : ARRAY[1..3] OF REAL := [0.005, 0, 0.022];
	eOffsetState : (CALIBRATE_OFFSET_TRIGGER, COLLECT_OFFSET_SAMPLE, CALIBRATE_OFFSET, OFFSET_ON, OFFSET_OFF);
	nCounter : INT;
	nInnerLoopCounter : UINT;
	
	// Offset variables
	fWrenchNotCompensated : ARRAY[1..6] OF REAL; // [N and Nm]
	fWrenchOffset : ARRAY[1..6] OF REAL; // [N and Nm]
	fWrenchOffsetSamples : ARRAY[1..6,1..OFFSET_SAMPLES_SIZE] OF REAL; // [N and Nm]
	nSampleCounter : UINT := 1;
	
	fFrameOffsetAngle : REAL := 3; // Difference between Meca and Medusa frame.
	fWrenchGravity : ARRAY[1..6] OF REAL; // [N and Nm]
	fWrenchGravityScaling : ARRAY[1..6] OF REAL := [0.95, 0.92, 1.0, 1.0, 1.0, 1.0];
	FG_WRF : ARRAY[1..3] OF REAL;
	FG_TRF : ARRAY[1..3] OF REAL;
	TG_TRF : ARRAY[1..3] OF REAL;
	xyzRot : ARRAY[1..3] OF REAL;
	
	
	r_x : ARRAY[1..3,1..3] OF REAL;
	r_y : ARRAY[1..3,1..3] OF REAL;
	r_z : ARRAY[1..3,1..3] OF REAL;
	alpha : REAL;
	beta : REAL;
	gamma : REAL;
	
	vTemp : ARRAY[1..3] OF REAL; // Vector
END_VAR
VAR CONSTANT
	OFFSET_SAMPLES_SIZE : UINT := 250;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Wrench from load
FG_WRF[3] := -9.81*fMass;
xyzRot[1] := EEPose[4];
xyzRot[2] := EEPose[5];
xyzRot[3] := EEPose[6] + fFrameOffsetAngle;

RotateVecFromWRFtoTRF(xyz_rot := xyzRot, vIn := FG_WRF, vOut => FG_TRF);

VecCrossVec(v1 := fCoM, v2 :=FG_TRF, vOut => TG_TRF);
fWrenchGravity[1] := FG_TRF[1];
fWrenchGravity[2] := FG_TRF[2];
fWrenchGravity[3] := FG_TRF[3];
fWrenchGravity[4] := TG_TRF[1];
fWrenchGravity[5] := TG_TRF[2];
fWrenchGravity[6] := TG_TRF[3];

// Offset state
CASE eOffsetState OF
	CALIBRATE_OFFSET_TRIGGER:
		nSampleCounter := 1;
		eOffsetState := COLLECT_OFFSET_SAMPLE;
	COLLECT_OFFSET_SAMPLE:
		FOR nCounter := 1 TO 6 DO
			fWrenchOffsetSamples[nCounter, nSampleCounter] := WrenchIn[nCounter] - fWrenchGravityScaling[nCounter]*fWrenchGravity[nCounter];
		END_FOR
		nSampleCounter := nSampleCounter + 1;
		IF nSampleCounter > OFFSET_SAMPLES_SIZE THEN
			eOffsetState := CALIBRATE_OFFSET;
		END_IF
	CALIBRATE_OFFSET:
		FOR nCounter := 1 TO 6 DO
			fWrenchOffset[nCounter] := 0.0;
			FOR nInnerLoopCounter := 1 TO OFFSET_SAMPLES_SIZE DO
				fWrenchOffset[nCounter] := fWrenchOffset[nCounter] + fWrenchOffsetSamples[nCounter, nInnerLoopCounter]/UINT_TO_REAL(OFFSET_SAMPLES_SIZE);
			END_FOR
		END_FOR
		eOffsetState := OFFSET_ON;
	OFFSET_ON:
		// Do nothing
	OFFSET_OFF:
		FOR nCounter := 1 TO 6 DO
			fWrenchOffset[nCounter] := 0.0;
		END_FOR
END_CASE

FOR nCounter := 1 TO 6 DO
	WrenchOut[nCounter] := WrenchIn[nCounter] - fWrenchOffset[nCounter] - fWrenchGravityScaling[nCounter]*fWrenchGravity[nCounter];
	fWrenchNotCompensated[nCounter] := WrenchIn[nCounter] - fWrenchOffset[nCounter];
END_FOR
]]></ST>
    </Implementation>
    <Method Name="CalibrateOffset" Id="{413293a9-c631-498e-8a87-216ef7b68e65}">
      <Declaration><![CDATA[METHOD CalibrateOffset
]]></Declaration>
      <Implementation>
        <ST><![CDATA[eOffsetState := CALIBRATE_OFFSET_TRIGGER;]]></ST>
      </Implementation>
    </Method>
    <Method Name="SetInertiaParameter" Id="{07ec7f80-81d3-4308-b59d-799df066ca18}">
      <Declaration><![CDATA[METHOD SetInertiaParameter
VAR_INPUT
	fMassNew : REAL;
	fCoMNew : ARRAY[1..3] OF REAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[fMass := fMassNew;
fCoM := fCoMNew;]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="ToolLoadCompensation">
      <LineId Id="96" Count="0" />
      <LineId Id="83" Count="5" />
      <LineId Id="108" Count="0" />
      <LineId Id="89" Count="6" />
      <LineId Id="82" Count="0" />
      <LineId Id="17" Count="3" />
      <LineId Id="54" Count="0" />
      <LineId Id="22" Count="21" />
      <LineId Id="9" Count="0" />
      <LineId Id="80" Count="0" />
      <LineId Id="57" Count="1" />
      <LineId Id="192" Count="0" />
      <LineId Id="55" Count="0" />
      <LineId Id="236" Count="0" />
    </LineIds>
    <LineIds Name="ToolLoadCompensation.CalibrateOffset">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="ToolLoadCompensation.SetInertiaParameter">
      <LineId Id="5" Count="0" />
      <LineId Id="8" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>